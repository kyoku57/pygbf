from enum import Enum
from .connector import Connector
import logging

class Channel(Enum):
    """ List of channel available """
    CHANNEL_1 = 'M'
    CHANNEL_2 = 'F'

class Parameter(Enum):
    """ List of functionality available to edit/read waves"""
    WAVEFORM        = 'W'
    FREQUENCY       = 'F'
    AMPLITUDE       = 'A'
    OFFSET          = 'O'
    DUTY_CYCLE      = 'D'
    PHASE           = 'P'
    TOGGLE_ON_OFF   = 'N'

class WaveForm(Enum):
    """ List of wave forms available """
    SINUS           = '00'
    SQUARE          = '01'
    CMOS            = '02'
    ADJ_PULSE       = '03'
    DC              = '04'
    TRIANGLE        = '05'
    RAMP            = '06'
    NEG_RAMP        = '07'
    STRAIR_TRIANGLE = '08'
    STAIR_STEP      = '09'
    NEG_STAIR       = '10'
    POS_EXPONEN     = '11'
    NEG_EXPONEN     = '12'

class SignalFactory:
    """ Main class to build signal """

    AMPLITUDE_MAX_VALUE = 10.0000

    def __init__(self, connector: Connector=None):
        """constructor
        :param connector: Connector object to interact with device
        """
        self.connector = connector
        self.channel = Channel.CHANNEL_1
        self.instructions = []


    def get_channel(self, channel: Channel):
        """get default or the one set channel
        :param channel: Channel or None
        if None, instance value is used
        """
        if channel is None:
            return self.channel.value
        else:
            return channel.value

    def _process(self, instruction):
        """ process instruction
        :param instruction: ascii string with the command from device documentation
        """
        if self.connector is not None:
            if self.connector.service.is_open:
                response = self.connector.write(instruction)
            else:
                response ="CONNECTOR_IS_CLOSED"
        else:
            response = "CONNECTOR_IS_NULL"
        self.instructions.append((instruction, response))
        logging.debug('Add entry ', (instruction, response))
        return response


    def set_waveform(self, waveform: WaveForm, channel: Channel=None):
        """Select Waveform
        :param waveform: WaveForm enum
        :param channel: Channel enum
        """
        instruction = 'W' + self.get_channel(channel) + Parameter.WAVEFORM.value  + waveform.value
        self._process(instruction)


    def get_waveform(self, channel: Channel=None):
        """Get Waveform
        :param channel: Channel enum
        """
        instruction = 'R' + self.get_channel(channel) + Parameter.WAVEFORM.value
        response = self._process(instruction)
        return WaveForm(response[:-1].zfill(2))


    def set_amplitude(self, amplitude: float, channel: Channel=None):
        """Set Amplitude 
        :param amplitude: float value of the amplitude in Volts (4 decimals accepted)
        :param channel: Channel enum
        """
        if type(amplitude) is not float:
            raise Exception('amplitude is not well formated. Use float')
        if amplitude < 0.0 or amplitude > 10.0:
            raise Exception('amplitude must be between 0.0V and 10.0V')

        amplitude_value = '{:.4f}'.format(amplitude).zfill(7)
        instruction = 'W' + self.get_channel(channel) + Parameter.AMPLITUDE.value + amplitude_value
        self._process(instruction)


    def get_amplitude(self, channel: Channel=None):
        """Get amplitude
        :param channel: Channel enum
        :return: float value
        """
        instruction = 'R' + self.get_channel(channel) + Parameter.AMPLITUDE.value
        response = self._process(instruction)
        return float(response[:-1])/ 10000


    def set_frequency(self, frequency: float, channel: Channel=None):
        """Set Frequency 
        :param frequency: float value of the frequency in Hertz (3 decimals accepted)
        :param channel: Channel enum
        """
        if type(frequency) is not float:
            raise Exception('frequency is not well formated. Use float')
        if frequency < 0.0 or frequency > 30000000.0:
            raise Exception('frequency must be between 0.0Hz and 30.0MHz')

        frequency_value = str(int(frequency*1e6)).zfill(14)
        instruction = 'W' + self.get_channel(channel) + Parameter.FREQUENCY.value + frequency_value
        self._process(instruction)


    def get_frequency(self, channel: Channel=None):
        """Get frequency
        :param channel: Channel enum
        :return: float value Hz
        """
        instruction = 'R' + self.get_channel(channel) + Parameter.FREQUENCY.value
        response = self._process(instruction)
        return float(response[:-1])