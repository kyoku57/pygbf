import serial

class Connector:
    """ this class permits connection to serial service"""

    def __init__(self, port="/dev/ttyUSB0"):
        """ Use the default connection to /dev/ttyUSB0 """
        self.port = port
        self.service = self.open(port)
        self.last_raw_request = b''
        self.last_raw_response = b''
        self.last_request=''
        self.last_response=0

    def open(self, port="/dev/ttyUSB0"):
        """ Open serial port 
        :param port: name of the port to use
        :return: serial.Serial service object
        """
        print('Open serial port : ' + port)
        self.port = port
        self.service = serial.Serial(port=port, 
                                baudrate=115200, 
                                parity=serial.PARITY_NONE, 
                                stopbits=serial.STOPBITS_ONE, 
                                bytesize=serial.EIGHTBITS, 
                                timeout=1)
        return self.service

    def write_raw(self, instruction: bytes):
        """Write RAW instruction to the port and get result
        :param instruction: bytes
        :return: bytes
        """
        self.service.write(instruction)
        response = self.service.read(35)
        return response

    def write(self, instruction: str):
        """Write instruction to the port and get result in a 30 bytes response
        :param instruction: str without end command
        :return: ascii chain
        """
        instruction_with_end_character = instruction + chr(0x0a)
        instruction_bytes = instruction_with_end_character.encode('ascii')
        response = self.write_raw(instruction_bytes)
        response_ascii = response.decode('ascii')

        self.last_request = instruction
        self.last_response = response_ascii[:-1]
        return response_ascii[:-1]

    def close(self):
        """ close port """
        print('Close serial port : ' + self.port)
        self.service.close()




"""
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
ser.read()
ser.write("WMW00")
ser.write(b'WMW00')
ser.write(b'WMW01')
ser.close()
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
ser.write(b'WMW01')
ser.write(b'WMW02')
ser
ser.write(b'WMW020x0a')
ser.write(b'WMW020x0a')
print('0x0a')
print(h'0x0a')
ser.write(b'WMW02\n')
ser.write(0x0a)
ser.write(b'WMW02'+0x0a)
hex(ser.write(b'WMW02))
hex(ser.write(b'WMW02'))
hex(ser.write(0x0a))
hex(ser.write(0x0b))
hex(ser.write(0x0a))
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
serial.STOPBITS_ONE
serial.STOPBITS_TWO
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
hex(ser.write(b'WMW02'))
hex(ser.write(b'WMW02\n'))
ser.close()
ser.write(b'test')
hex('\n')
hex(b'\n')
hex(b'test')
hex(b'0x0a')
0x0a
bin(10)
0b1010
bin('\n')
0x0a
bin(0x0a)
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)4
ser = serial.Serial(port="/dev/ttyUSB0", baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
ser.write(b'WMW01'+bin(0x0a))
ser.write((b'WMW01')+bin(0x0a))
ser.write('test')
b'test'
byte = b'test'
str(byte)
input = raw_input(">>")
ser.
ser
ser.write('\r')
ser.write(b'\r')
ser.write(b'\rWMW01\r')
ser.write(b'\rWMW03\r')
ser.write(b'\rWMW03\n')
ser.write(b'\nWMW03\n')
ser.write(b'\nWMW02\n')
ser.write(b'\nWMW01\n')
ser.write(b'WMW01\n')
ser.write(b'WMW02\n')
ser.write(b'WMW01\n')
ser.write(b'WMW00\n')
ser.write(b'WMW02\n')
hex(ser.write(b'WMW02\n'))
hex(ser.write(b'WMW04\r'))
hex(ser.write(b'WMW02\r'))
hex(ser.write(b'WMW02\n'))
hex(ser.write(b'WMW01\n'))
hex(ser.write(b'WMW00\n'))
hex(ser.write(b'WMW94\n'))
hex(ser.write(b'WMW96\n'))
hex(ser.write(b'WMW97\n'))
hex(ser.write(b'WMW98\n'))
hex(ser.write(b'WMW99\n'))
hex(ser.write(b'WMW00\n'))
hex(ser.write(b'WMF10000000000000\n'))
hex(ser.write(b'WMF12340000000000\n'))
%save
%save experience.py
%save .experience.py
%save.experience.py
%save experience.py
%save -r experience 
%save -r experience 1-9999999
hex(ser.write(b'WMA0952\n'))
hex(ser.write(b'WMA0010\n'))
hex(ser.write(b'WMA005\n'))
hex(ser.write(b'WMA001\n'))
hex(ser.write(b'WMA01.25\n'))
hex(ser.write(b'WMW01\n'))
hex(ser.write(b'WMD20.5\n'))
hex(ser.write(b'WFW01\n'))
hex(ser.write(b'WMW01\n'))
ser.close()
%save -r session 1-99999999999999
"""