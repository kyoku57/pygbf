# pygbf - a simple library to communication with FY6600-30M

This a simple module to help interactions with FY6600-30M device.

![fy6600-30](docs/fy6600-30m.jpg)

## How to play in a sandbox with ipython and dev mode ?

```bash
$ pipenv install --dev
$ pipenv run ipython
```

for debugging, you can set these commands at the start of iPython

```python
# to autor reload module after editing 
%load_ext autoreload
%autoreload 2

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
```

## Usage of the library

The library contains two submodules:

- **connector.py** : a low level module to intiate communication with FY6600 devices
- **functions.py** : a higher level module to interact with FY6600-30M with the correct messages exchanges. It needs a *connector.Connector()* object


```python
from pygbf.connector import Connector
from pygbf.functions import *

# initiate a connection
c = Connector()  # equivalent to : c = Connector('/dev/ttyUSB0') 

# initiate signal factory
sf = SignalFactory(c)

# PC --> Device
sf.channel = Channel.CHANNEL_1      # or Channel.CHANNEL_2
sf.set_waveform(WaveForm.SQUARE)    # see WaveForm Enum class
sf.set_waveform(WaveForm.SINUS) 
sf.set_amplitude(5.2345)            # value in V
sf.set_frequency(440)               # value in Hz

# Device --> PC
sf.get_waveform()   # <WaveForm.SINUS: '00'>
sf.get_amplitude()  # 5.2345

# close connection
c.close()
```